<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class NewsControllerTest extends WebTestCase {

    const API_URI = '/api/news';

    /** @var  Client */
    private $client;

    protected function setUp() {
        parent::setUp();
        $this->client = static::createClient();
        $kernel = $this->client->getKernel();
        $app = new Application($kernel);

        // Drop database
        $command = $app->find('doctrine:database:drop');
        $input = new ArrayInput(['--force' => true]);
        $command->run($input, new ConsoleOutput());

        // Create database
        $command = $app->find('doctrine:database:create');
        $command->run(new ArrayInput([]), new ConsoleOutput());

        // Migrate
        $command = $app->find('doctrine:migrations:migrate');
        $input = new ArrayInput([]);
        $input->setInteractive(false);
        $command->run($input, new ConsoleOutput());
    }

    public function testCreate() {
        $client = $this->client;
        $sampleData = $this->getNewsSampleData();
        $crawler = $client->request('POST', self::API_URI, [], [], [], json_encode($sampleData));

        $this->assertSame(201, $client->getResponse()->getStatusCode());

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue(json_last_error() === JSON_ERROR_NONE, 'Response should be valid JSON string');
        $this->assertArrayHasKey('id', $response, 'There is no ID. Entity was not created.');

        $this->assertArrayHasKey('title', $response, 'There is no title property in the response');
        $this->assertSame($sampleData['title'], $response['title']);

        $this->assertArrayHasKey('content', $response, 'There is no content property in the response');
        $this->assertSame($sampleData['content'], $response['content']);
    }

    private function getNewsSampleData() {
        return [
            'title' => 'news title ' . md5(time() . rand()),
            'content' => 'news content ' . md5(time() . rand()),
        ];
    }

}
