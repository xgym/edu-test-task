# Установка

Для развертывания приложения, необходимо выполнить последовательно в командной строке:

- git clone https://xgym@bitbucket.org/xgym/edu-test-task.git
- cd edu-test-task
- composer install
- rm -Rf ./var/cache
- php bin/console cache:clear
- php bin/console doctrine:database:create
- php bin/console doctrine:migrations:migrate
- php -S 127.0.0.1:8080 -t ./public

# Запуск тестов

В корне проекта:

`./vendor/bin/simple-phpunit`

# Методы API

## Добавление

POST /api/news

В теле запроса должны быть передена в формате JSON: `title` и `content`.

## Обновление

PUT /api/news/{id}

{id} - идентификатор новости

В теле запроса должны быть передена в формате JSON: `title` или `content`.

## Просмотр новости

GET /api/news/{id}

{id} - идентификатор новости

## Удаление новости

DELETE /api/news/{id}

{id} - идентификатор новости

## Просмотр списка новостей

GET /api/news

Доступны фильтры через `queryString`: 

- `dateStart` 
- `dateEnd`

Формат дат: 'YYYY-mm-dd hh:ii:ss'

Пагинация:

- `limit` - максимальное количество
- `offset` - смещение

Ответ: массив с ключами `items`, `total` (количество новостей соответствующих фильтру).
