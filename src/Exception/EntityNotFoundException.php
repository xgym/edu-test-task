<?php

namespace App\Exception;

class EntityNotFoundException extends \Exception {
    protected $message = 'Not found';
}
