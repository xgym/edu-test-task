<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception {

    /** @var  ConstraintViolationListInterface */
    private $violations;

    public function __construct(
        ConstraintViolationListInterface $violationList,
        $message = '',
        $code = 0,
        \Throwable $previousException = null
    ) {
        parent::__construct($message, $code, $previousException);
        $this->violations = $violationList;
    }

    /**
     * @return array
     */
    public function getViolations(): array {
        $violations = [];
        /** @var ConstraintViolation $violation */
        foreach ($this->violations as $violation) {
            $violations[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $violations;
    }

}
