<?php

namespace App\Model\SearchFilter;

use App\Entity\News;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\QueryBuilder;

class NewsSearchFilter implements SearchFilterInterface {

    const MAX_LIMIT_SIZE = 20;

    private $startDate;
    private $endDate;
    private $limit = self::MAX_LIMIT_SIZE;
    private $offset = 0;

    public function fillFromData(array $filterData) {
        $filterData = array_map('filter_var', $filterData);

        if (array_key_exists('startDate', $filterData)) {
            try {
                $this->startDate = new \DateTime($filterData['startDate']);
            }
            catch (\Exception $e) {
                $this->startDate = $filterData['startDate'];
            }
        }

        if (array_key_exists('endDate', $filterData)) {
            try {
                $this->endDate = new \DateTime($filterData['endDate']);
            }
            catch (\Exception $e) {
                $this->endDate = $filterData['endDate'];
            }
        }

        if (array_key_exists('limit', $filterData)) {
            $this->limit = (int)$filterData['limit'];
        }

        if (array_key_exists('offset', $filterData)) {
            $this->offset = (int)$filterData['offset'];
        }
    }

    public function apply(QueryBuilder $qb) {
        if (!$qb->getDQLPart('select')) {
            $qb->select('news');
        }

        $qb
            ->from(News::class, 'news')
            ->setFirstResult($this->offset)
            ->setMaxResults($this->limit);

        if ($this->startDate) {
            $qb->andWhere($qb->expr()->gte('news.createdAt', ':startDate'));
            $qb->setParameter('startDate', $this->startDate, Type::DATETIME);
        }

        if ($this->endDate) {
            $qb->andWhere($qb->expr()->lte('news.createdAt', ':endDate'));
            $qb->setParameter('endDate', $this->endDate, Type::DATETIME);
        }

        $qb->orderBy(
            'news.createdAt',
            'DESC'
        );
    }

}
