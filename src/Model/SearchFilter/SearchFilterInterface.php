<?php

namespace App\Model\SearchFilter;

use Doctrine\ORM\QueryBuilder;

interface SearchFilterInterface {
    public function fillFromData(array $filterData);
    public function apply(QueryBuilder $qb);
}
