<?php

namespace App\Controller;

use App\Service\NewsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * @Route("/api/news")
 *
 * Class NewsController
 * @package App\Controller
 */
class NewsController extends Controller {

    /**
     * @Route("")
     * @Method("GET")
     *
     * @param Request $request
     * @param NewsService $newsService
     * @return JsonResponse
     */
    public function index(Request $request, NewsService $newsService): JsonResponse {
        $filter = $request->query->all();

        $newsList = $newsService->queryList($filter);
        $total = $newsService->queryCount($filter);

        return new JsonResponse([
            'items' => $newsList,
            'total' => $total,
        ]);
    }

    /**
     * @Route("")
     * @Method("POST")
     *
     * @param Request $request
     * @param NewsService $newsService
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $request, NewsService $newsService): JsonResponse {
        $requestContent = (array)json_decode($request->getContent(), true);

        $news = $newsService->doCreate($requestContent);

        return new JsonResponse($news, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("/{newsId}", requirements={"newsId"="\d+"})
     * @Method("PUT")
     *
     * @param $newsId
     * @param NewsService $newsService
     * @param Request $request
     * @return JsonResponse
     */
    public function update($newsId, NewsService $newsService, Request $request): JsonResponse {
        $requestContent = (array)json_decode($request->getContent(), true);

        $news = $newsService->doUpdate($newsId, $requestContent);

        return new JsonResponse($news);
    }

    /**
     * @Route("/{newsId}", requirements={"newsId"="\d+"})
     * @Method("DELETE")
     *
     * @param $newsId
     * @param NewsService $newsService
     * @return JsonResponse
     */
    public function delete($newsId, NewsService $newsService): JsonResponse {
        $newsService->doDelete($newsId);

        return new JsonResponse('deleted');
    }

    /**
     * @Route("/{newsId}", requirements={"newsId"="\d+"})
     * @Method("GET")
     *
     * @param $newsId
     * @param NewsService $newsService
     * @return JsonResponse
     */
    public function one($newsId, NewsService $newsService): JsonResponse {
        return new JsonResponse($newsService->queryOne($newsId));
    }

}
