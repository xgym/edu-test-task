<?php

namespace App\EventSubscriber;

use App\Exception\EntityNotFoundException;
use App\Exception\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelEventSubscriber implements EventSubscriberInterface {

    public function onKernelException(GetResponseForExceptionEvent $event) {
        $responseContent = null;
        $httpCode = JsonResponse::HTTP_BAD_REQUEST;
        $exception = $event->getException();
        switch (true) {
            case $exception instanceof ValidationException:
                $responseContent = $exception->getViolations();
                break;
            case $exception instanceof EntityNotFoundException:
                $responseContent = $exception->getMessage();
                $httpCode = JsonResponse::HTTP_NOT_FOUND;
                break;
            default:
                $responseContent = $exception->getMessage() ?? 'Unknown error';
        }

        $response = new JsonResponse($responseContent, $httpCode);
        $event->setResponse($response);
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::EXCEPTION => ['onKernelException']
        ];
    }

}
