<?php

namespace App\Entity;

class News implements EntityInterface, \JsonSerializable {
    /** @var int */
    private $id;
    /** @var string */
    private $title;
    /** @var string */
    private $content;
    /** @var \DateTime */
    private $createdAt;
    /** @var \DateTime */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return News
     */
    public function setId(int $id): News
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): News
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return News
     */
    public function setContent(string $content): News
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt(\DateTime $updatedAt): News
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * В реальном приложении необходимо использовать отдельный модуль сериализации и библиотеку для очистки данных
     *
     * @return array
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'title' => htmlspecialchars($this->title),
            'content' => htmlspecialchars($this->content),
            'createdAt' => $this->createdAt->format('d.m.Y h:i'),
            'updatedAt' => $this->updatedAt->format('d.m.Y h:i'),
        ];
    }

}
