<?php

namespace App\Service;

use App\Entity\EntityInterface;
use App\Entity\News;
use App\Exception\EntityNotFoundException;
use App\Exception\ValidationException;
use App\Model\SearchFilter\NewsSearchFilter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NewsService implements ServiceInterface
{
    /** @var ValidatorInterface */
    private $validator;
    /** @var EntityManager */
    private $entityManager;
    /** @var EntityRepository */
    private $newsRepository;

    /**
     * NewsService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ) {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->newsRepository = $entityManager->getRepository(News::class);
    }

    /**
     * @param array $newsData
     * @return News
     * @throws \Exception
     */
    public function doCreate(array $newsData): News {
        $news = new News();
        $news
            ->setTitle($newsData['title'] ?? '')
            ->setContent($newsData['content'] ?? '');

        $this->validate($news);

        $this->entityManager->persist($news);
        $this->entityManager->flush($news);

        return $news;
    }

    public function doUpdate($newsId, array $newsData) {
        if (!$newsData) {
            throw new \Exception('No changes');
        }

        $news = $this->ensureNewsExists($newsId);
        $newsOriginalHash = md5(json_encode($news));
        if (array_key_exists('title', $newsData)) {
            $news->setTitle($newsData['title']);
        }

        if (array_key_exists('content', $newsData)) {
            $news->setContent($newsData['content']);
        }

        if ($newsOriginalHash === md5(json_encode($news))) {
            throw new \Exception('No changes');
        }

        $this->validate($news);

        $news->setUpdatedAt(new \DateTime());
        $this->entityManager->flush($news);

        return $news;
    }

    /**
     * @param $newsId
     * @return EntityInterface
     * @internal param EntityInterface $news
     */
    public function doDelete($newsId) {
        $news = $this->ensureNewsExists($newsId);
        $this->entityManager->remove($news);
        $this->entityManager->flush();

        return $news;
    }

    public function queryList($filterData) {
        $filter = new NewsSearchFilter();
        $filter->fillFromData($filterData);
        $this->validate($filter);

        $qb = $this->entityManager->createQueryBuilder();
        $filter->apply($qb);

        return $qb->getQuery()->getResult();
    }

    public function queryCount($filterData) {
        $filter = new NewsSearchFilter();
        $filter->fillFromData($filterData);
        $this->validate($filter);

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('COUNT(news)');

        $filter->apply($qb);

        $qb
            ->setFirstResult(0)
            ->setMaxResults(null);

        return (int)($qb->getQuery()->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR) ?? 0);
    }

    /**
     * @param $newsId
     * @return News
     */
    public function queryOne($newsId) {
        return $this->ensureNewsExists($newsId);
    }

    /**
     * @param $newsId
     * @return News
     * @throws EntityNotFoundException
     */
    protected function ensureNewsExists($newsId) {
        $news = $this->newsRepository->find($newsId);
        if ($news) {
            return $news;
        }

        throw new EntityNotFoundException('News not found');
    }

    /**
     * @param $news
     * @throws ValidationException
     */
    protected function validate($news) {
        $violations = $this->validator->validate($news);
        if ($violations->count()) {
            throw new ValidationException($violations);
        }
    }
}
