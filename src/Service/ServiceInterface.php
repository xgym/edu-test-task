<?php

namespace App\Service;

interface ServiceInterface {

    /* Command */
    public function doCreate(array $entityData);
    public function doUpdate($entityId, array $entityData);
    public function doDelete($entityId);

    /* Query */
    public function queryList($filter);
    public function queryOne($entityId);
    public function queryCount($filter);

}
